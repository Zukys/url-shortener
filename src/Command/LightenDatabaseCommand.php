<?php

namespace App\Command;

use App\Entity\Link;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LightenDatabaseCommand extends Command
{
	const KEEP_COUNT = 20;

	protected static $defaultName = 'app:lighten-database';

	/**
	 * @var EntityManagerInterface $em
	 */
    private $em;

	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;

		parent::__construct();
    }

	protected function configure()
	{
		$this
			->setDescription('Lighten the database by deleting some data')
			->addArgument('count', InputArgument::OPTIONAL, 'Count of data to keep')
		;
	}

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
		$linkRepository = $this->em->getRepository(Link::class);
		$data = $linkRepository->findAll();

		$keepCount = self::KEEP_COUNT;
		$countArg = $input->getArgument('count');
		if ($countArg) {
			$keepCount = $countArg;
		}
		$output->writeln('Count of data to keep: '.$keepCount);

		while(count($data) > $keepCount) {
			$oldestObject = array_shift($data);
			$this->em->remove($oldestObject);
			$this->em->flush();
		}
		$output->writeln('Database successfully lighten to '.$keepCount.' objects.');

		return Command::SUCCESS;
	}
}
