<?php

namespace App\Controller;

use App\Entity\Link;
use App\Service\LinkHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class LinkController extends AbstractController
{
	/**
	 * @var LinkHelper $linkHelper
	 */
	private $linkHelper;

	public function __construct(LinkHelper $linkHelper)
	{
		$this->linkHelper = $linkHelper;
	}

	/**
	 * @Route("/generate-link", name="generate_link")
	 */
	public function generateLink(Request $request): Response
	{
		$link = new Link();
		$form = $this->createFormBuilder($link)
			->add('originLink', UrlType::class, ['label' => 'Enter link to be shorten: '])
			->add('generate', SubmitType::class, ['label' => 'Generate short'])
			->getForm()
		;

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			/** @var Link $link */
			$link = $form->getData();

			try {
				get_headers($link->getOriginLink());
				$shortLink = $this->linkHelper->generateLinkUniqueId();
				$link->setShortUniqueId($shortLink);

				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($link);
				$entityManager->flush();

				return $this->redirectToRoute('short_link', [
					'shortUniqueId' => $shortLink,
				]);
			} catch(\Exception $e) {
				$invalidLink = $request->get('form')['originLink'];
			}
		} else {
			$invalidLink = null;
		}

		return $this->render('link/generate_link.html.twig', [
			'form' => $form->createView(),
			'invalidLink' => $invalidLink,
		]);
	}

	/**
	 * @Route("/shorten-link/{shortUniqueId}", name="short_link")
	 */
	public function shortenLink(Request $request, string $shortUniqueId): Response
	{
		/* @var Link $link */
		$link = $this->linkHelper->findLinkByShortUniqueId($shortUniqueId);
		$shortLink = null;

		if ($link) {
			$shortLinkRoute = $this->generateUrl('redirect_shorten_link', [
				'shortUniqueId' => $link->getShortUniqueId(),
			]);
			$baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
			$shortLink = $baseUrl.$shortLinkRoute;
		}

		return $this->render('link/shorten_link.html.twig', [
			'link' => $shortLink,
		]);
	}

	/**
	 * @Route("/redirect/{shortUniqueId}", name="redirect_shorten_link")
	 */
	public function redirectShortenLink(string $shortUniqueId)
	{
		/* @var Link $link */
		$link = $this->linkHelper->findLinkByShortUniqueId($shortUniqueId);

		if (!$link) {

			return $this->redirectToRoute('short_link', [
				'shortUniqueId' => $shortUniqueId,
			]);
		}

		return $this->redirect($link->getOriginLink());
	}

	/* In progress */
	/**
	 * @Route("/encode", name="encode")
	 */
	public function encode()
	{
		return new JsonResponse([
			'testEncode' => "testEncode",
		]);
	}

	/* In progress */
	/**
	 * @Route("/decode", name="decode")
	 */
	public function decode()
	{
		return new JsonResponse([
			'testDecode' => "testDecode",
		]);
	}

	/* In progress */
	/**
	 * @Route("/link-api", name="link_api")
	 */
	public function linkApi()
	{
		$links = $this->getDoctrine()->getRepository(Link::class)->findAll();

		$encoders = [new JsonEncoder()];
		$normalizers = [new DateTimeNormalizer(), new ObjectNormalizer()];
		$serializer = new Serializer($normalizers, $encoders);

		$jsonContent = $serializer->serialize($links, 'json');

		$response = JsonResponse::fromJsonString($jsonContent);

		$response->setEncodingOptions( $response->getEncodingOptions() | JSON_PRETTY_PRINT );

		return $response;
	}
}
