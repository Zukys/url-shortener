<?php

namespace App\Entity;

use App\Repository\LinkRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LinkRepository::class)
 */
class Link
{
    /**
	 * @var int $id
	 *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @var \DateTime $createdAt
	 *
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $createdAt;

    /**
	 * @var string $shortUniqueId
	 *
     * @ORM\Column(type="string", length=5, nullable=true)
     */
	private $shortUniqueId;

    /**
	 * @var string $originLink
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $originLink;

	public function __construct()
	{
		$this->createdAt = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

	public function getCreatedAt(): ?\DateTime
	{
		return $this->createdAt;
	}

	public function setCreatedAt(?\DateTime $createdAt): void
	{
		$this->createdAt = $createdAt;
	}

    public function getShortUniqueId(): ?string
    {
        return $this->shortUniqueId;
    }

    public function setShortUniqueId(?string $shortUniqueId): void
    {
        $this->shortUniqueId = $shortUniqueId;
    }

    public function getOriginLink(): ?string
    {
        return $this->originLink;
    }

    public function setOriginLink(?string $originLink): void
    {
        $this->originLink = $originLink;
    }
}
