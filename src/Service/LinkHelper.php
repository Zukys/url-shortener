<?php

namespace App\Service;

use App\Repository\LinkRepository;

class LinkHelper
{
	/**
	 * @var LinkRepository $linkRepository
	 */
	private $linkRepository;

	public function __construct(LinkRepository $linkRepository) {
		$this->linkRepository = $linkRepository;
	}

	public function generateLinkUniqueId()
	{
		$shortUniqueId = null;
		while ($shortUniqueId === null || $this->linkRepository->findOneBy(["shortUniqueId" => $shortUniqueId])) {
			$randomness = uniqid();
			$randomness = md5($randomness);
			$shortUniqueId = substr($randomness, 0, 5);
		}

		return $shortUniqueId;
	}

	public function findLinkByShortUniqueId(string $shortLink)
	{
		return  $this->linkRepository->findOneBy(["shortUniqueId" => $shortLink]);
	}
}
